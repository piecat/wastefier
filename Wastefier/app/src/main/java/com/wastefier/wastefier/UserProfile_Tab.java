package com.wastefier.wastefier;

/**
 * Created by Sully the Seal on 08/05/2017.
 * Edited by CAASI on 08/19/2017
 */
import android.content.Context;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

public class UserProfile_Tab extends Fragment{
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private DatabaseReference mDatabase;
    private DatabaseReference questone;
    private DatabaseReference questtwo;

    public UserAccount user_acc;
    public TextView textUsername;
    public TextView textEmail;
    public TextView textScore;
    public String score_s;

    //  Quests
    public TextView quest1;
    public TextView quest2;
    public String quest_one;
    public String quest_two;

    public String score;
    public String item;
    boolean flag;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle temp = getArguments();
        if(temp == null){
            Log.e("EMPTY","TEMP");
        }
        if(temp != null && temp.getString("score")!= null && temp.getString("item")!=null) {
            Log.e("INSIDE","THERE IS");
            score = temp.getString("score");
            item = temp.getString("item");
            Log.e("[3]Item:Score",item+":"+score);
            flag = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_1, container, false);
        flag = false;
        textUsername = (TextView) rootView.findViewById(R.id.username);
        textEmail = (TextView) rootView.findViewById(R.id.email);
        textScore = (TextView) rootView.findViewById(R.id.score);
        quest1 = (TextView) rootView.findViewById(R.id.quest1);
        quest2 = (TextView) rootView.findViewById(R.id.quest2);



        if(isNetworkAvailable(getContext())) {
            firebaseAuth = FirebaseAuth.getInstance();
            user = firebaseAuth.getCurrentUser();
            Log.e("USER UID", user.getUid());
            mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid());

            if(score_s != score){
                UserAccount new_user = new UserAccount(textUsername.toString(),score);
                mDatabase.child("User").child(user.getUid()).setValue(new_user);
            }

            Random r = new Random();
            if(flag) {
                if (item.toLowerCase() == quest_one.toLowerCase()) {
                    questone = FirebaseDatabase.getInstance().getReference().child("Quest").child(r.nextInt(10) + "");
                }
                if (item.toLowerCase() == quest_two.toLowerCase()) {
                    questtwo = FirebaseDatabase.getInstance().getReference().child("Quest").child(r.nextInt(10) + "");
                }
                flag = false;
            }
            else {
                questone = FirebaseDatabase.getInstance().getReference().child("Quest").child(r.nextInt(10) + "");

                questtwo = FirebaseDatabase.getInstance().getReference().child("Quest").child(r.nextInt(10) + "");
            }
            textEmail.setText(user.getEmail());

            Toast.makeText(this.getContext(), "Welcome " + user.getEmail(), Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this.getContext(),"No internet connection",Toast.LENGTH_LONG).show();
        }
        return rootView;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(conMan.getActiveNetworkInfo() != null && conMan.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onStart() {
        super.onStart();





        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("User",dataSnapshot.getValue().toString());
                UserAccount user_acc = dataSnapshot.getValue(UserAccount.class);
                textUsername.setText(user_acc.getName());
                textScore.setText("Score :" + user_acc.getScore());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        questone.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Quest",dataSnapshot.getValue().toString());
                Quest quest = dataSnapshot.getValue(Quest.class);
                quest_one = quest.getName();
                quest1.setText(quest.getName()+" "+quest.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        questtwo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("Quest",dataSnapshot.getValue().toString());
                Quest quest = dataSnapshot.getValue(Quest.class);
                quest_two = quest.getName();
                quest2.setText(quest.getName()+" "+quest.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
