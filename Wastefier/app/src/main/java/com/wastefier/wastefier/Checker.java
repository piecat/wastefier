package com.wastefier.wastefier;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import static java.lang.System.in;

/**
 * Created by user on 7/24/2017.
 */

public class Checker {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference bioDatabase1;
    private DatabaseReference bioDatabase2;
    private DatabaseReference bioDatabase3;
    private DatabaseReference bioDatabase4;
    private DatabaseReference bioDatabase5;
    private DatabaseReference bioDatabase6;
    private DatabaseReference nonbioDatabase1;
    private DatabaseReference nonbioDatabase2;
    private DatabaseReference nonbioDatabase3;
    private DatabaseReference nonbioDatabase4;
    private DatabaseReference nonbioDatabase5;
    private DatabaseReference nonbioDatabase6;
    private DatabaseReference questone;
    private DatabaseReference questtwo;
    private FirebaseUser user;


    ArrayList <String> biodegradable = new ArrayList<String>();
    ArrayList <String> nonbiodegradable= new  ArrayList <String>() ;

    public Checker(){

        initializeGarbage();

    }
    public String check(String string){
        String answer="Unknown";
        string = string.toLowerCase();

        ListIterator <String> t=  biodegradable.listIterator();
        for(int i=0;i<biodegradable.size();i++){
            //Log.e(string,biodegradable.get(i).toLowerCase());
            if(string.contains(biodegradable.get(i).toLowerCase())){
                answer = "Biodegradable";
               // Log.e("BIO","NonBiodegradable");
                break;
            }
        }
        for(int i=0;i<nonbiodegradable.size();i++){
            //Log.e(string,nonbiodegradable.get(i).toLowerCase());
            if(string.contains(nonbiodegradable.get(i).toLowerCase())){
                answer = "NonBiodegradable";
              //  Log.e("NON-BIO","NonBiodegradable");
                break;
            }
        }
        return answer;
    }

    public String common(String string){
        string = string.toLowerCase();
        for(int i=0;i<biodegradable.size();i++){
            if(string.contains(biodegradable.get(i).toLowerCase())) {
                return biodegradable.get(i);
            }
        }
        for(int i=0;i<nonbiodegradable.size();i++){
            if(string.contains(nonbiodegradable.get(i).toLowerCase())){
                return nonbiodegradable.get(i);
            }
        }
        return null;

    }

    private void initializeGarbage(){
        bioDatabase1 = FirebaseDatabase.getInstance().getReference().child("Biodegradable").child("Food");
        bioDatabase2 = FirebaseDatabase.getInstance().getReference().child("Biodegradable").child("Fruit");
        bioDatabase3 = FirebaseDatabase.getInstance().getReference().child("Biodegradable").child("Materials");
        bioDatabase4 = FirebaseDatabase.getInstance().getReference().child("Biodegradable").child("Others");
        bioDatabase5 = FirebaseDatabase.getInstance().getReference().child("Biodegradable").child("Snacks");
        bioDatabase6 = FirebaseDatabase.getInstance().getReference().child("Biodegradable").child("Vegetable");

        nonbioDatabase1 = FirebaseDatabase.getInstance().getReference().child("Nonbiodegradable").child("Appliance");
        nonbioDatabase2 = FirebaseDatabase.getInstance().getReference().child("Nonbiodegradable").child("Electronics");
        nonbioDatabase3 = FirebaseDatabase.getInstance().getReference().child("Nonbiodegradable").child("Materials");
        nonbioDatabase4 = FirebaseDatabase.getInstance().getReference().child("Nonbiodegradable").child("Medical");
        nonbioDatabase5 = FirebaseDatabase.getInstance().getReference().child("Nonbiodegradable").child("Others");
        nonbioDatabase6 = FirebaseDatabase.getInstance().getReference().child("Nonbiodegradable").child("School_Supplies");

        bioDatabase1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    //Log.e("BIODEGRADABLE",data.getValue().toString());
                    biodegradable.add(data.getValue(String.class));
                }
                //Log.e("food",biodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        bioDatabase2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("BIODEGRADABLE",data.getValue().toString());
                    biodegradable.add(data.getValue(String.class));
                }
                //Log.e("fruit",biodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        bioDatabase3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("BIODEGRADABLE",data.getValue().toString());
                    biodegradable.add(data.getValue(String.class));
                }
                //Log.e("materials",biodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        bioDatabase4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("BIODEGRADABLE",data.getValue().toString());
                    biodegradable.add(data.getValue(String.class));
                }
                //Log.e("others",biodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        bioDatabase5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    //Log.e("BIODEGRADABLE",data.getValue().toString());
                    biodegradable.add(data.getValue(String.class));
                }
               // Log.e("snacks",biodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        bioDatabase6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("BIODEGRADABLE",data.getValue().toString());
                    biodegradable.add(data.getValue(String.class));
                }
                //Log.e("vegetable",biodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        nonbioDatabase1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("NONBIODEGRADABLE",data.getValue().toString());
                    nonbiodegradable.add(data.getValue(String.class));
                }
               // Log.e("appliance",nonbiodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        nonbioDatabase2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    //Log.e("NONBIODEGRADABLE",data.getValue().toString());
                    nonbiodegradable.add(data.getValue(String.class));
                }
               // Log.e("electronics",nonbiodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        nonbioDatabase3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("NONBIODEGRADABLE",data.getValue().toString());
                    nonbiodegradable.add(data.getValue(String.class));
                }
              //  Log.e("materials",nonbiodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        nonbioDatabase4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("NONBIODEGRADABLE",data.getValue().toString());
                    nonbiodegradable.add(data.getValue(String.class));
                }
                //Log.e("medical",nonbiodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        nonbioDatabase5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                  //  Log.e("NONBIODEGRADABLE",data.getValue().toString());
                    nonbiodegradable.add(data.getValue(String.class));
                }
//                Log.e("others",nonbiodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        nonbioDatabase6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                   // Log.e("NONBIODEGRADABLE",data.getValue().toString());
                    nonbiodegradable.add(data.getValue(String.class));
                }
    //            Log.e("school_supplies",nonbiodegradable.toString());

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
