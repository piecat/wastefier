package com.wastefier.wastefier;

/**
 * Created by CAASI on 8/19/2017.
 *
 *  THIS CLASS USERACCOUNT IS USED FOR A MODEL IN FIREBASE
 */

public class UserAccount {
    public String name;
    public String score;

    public UserAccount(){
        name = "";
        score = "";
    }

    public UserAccount(String name , String score){
        this.name =name;
        this.score = score;
    }

    public String getName(){return this.name;};
    public String getScore(){return this.score;};
    public void setName(String name) {this.name = name;}
    public void setScore(String score) {this.score = score;}
}
