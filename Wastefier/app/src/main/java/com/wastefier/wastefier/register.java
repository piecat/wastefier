package com.wastefier.wastefier;

        import android.app.ProgressDialog;
        import android.content.Intent;
        import android.support.annotation.NonNull;
        import android.support.design.widget.TextInputLayout;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.text.TextUtils;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.Toast;

        import com.google.android.gms.tasks.OnCompleteListener;
        import com.google.android.gms.tasks.Task;
        import com.google.firebase.auth.AuthResult;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.auth.FirebaseUser;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;

public class register extends AppCompatActivity {
    TextInputLayout email;
    TextInputLayout password;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ImageView cancel = (ImageView) findViewById(R.id.cancel);
        ImageView signup = (ImageView) findViewById(R.id.signup);
        email = (TextInputLayout) findViewById(R.id.email);
        final TextInputLayout name = (TextInputLayout) findViewById(R.id.username);
        password = (TextInputLayout) findViewById(R.id.password);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser(name);
            }
        });
    }
    private void registerUser(final TextInputLayout name){
        //getting email and password from edit texts
        final String stremail = email.getEditText().getText().toString().trim();
        final String strpass  = password.getEditText().getText().toString().trim();
        final String strname = name.getEditText().getText().toString().trim();

        //checking if email and passwords are empty
        if(TextUtils.isEmpty(stremail)){
            Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
        }

        if(TextUtils.isEmpty(strpass)){
            Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
        }

        if(TextUtils.isEmpty(strname)){
            Toast.makeText(this,"Please enter username",Toast.LENGTH_LONG).show();
        }


        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registering Please Wait...");
        progressDialog.show();
        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(stremail, strpass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        Log.e("HERE","HERE");
                        if (task.isSuccessful()) {
                            //display some message here
                            Toast.makeText(register.this, "Successfully registered", Toast.LENGTH_LONG).show();
                            loginUser(strname,stremail,strpass);
                        } else {
                            //display some message here
                            Toast.makeText(register.this, "Registration error", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }
                });

    }

    private void loginUser(String temp,String stremail,String strpass){
        final String username = temp;
        firebaseAuth.signInWithEmailAndPassword(stremail,strpass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            Intent intent = new Intent(getApplicationContext(), registered.class);
                            intent.putExtra("message", username);
                            startActivity(intent);
                        }
                    }
                });

    }

}
