package com.wastefier.wastefier;

/**
 * Created by CAASI on 9/19/2017.
 */

public class Quest {
    String name;
    Long value;

    public Quest(String name, Long value) {
        this.name = name;
        this.value = value;
    }

    public Quest(){
        this.name = "";
        this.value = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
