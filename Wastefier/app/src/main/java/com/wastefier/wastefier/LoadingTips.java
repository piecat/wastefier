package com.wastefier.wastefier;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

public class LoadingTips extends AppCompatActivity {
    TextView tips;
    String answer;
    private DatabaseReference tipsDatabase;
    String item;
    String tip;
    byte[] byteArray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_tips);
        Bundle extras = getIntent().getExtras();
        byteArray = extras.getByteArray("picture");
        answer = extras.getString("answer");
        item=extras.getString("item");
        tips = (TextView) findViewById(R.id.tips);


        Random r = new Random();
        tipsDatabase = FirebaseDatabase.getInstance().getReference().child("Tip").child(""+r.nextInt(17));
        initializeTips();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainIntent = new Intent(LoadingTips.this, Segregation.class);
                LoadingTips.this.startActivity(mainIntent);
                LoadingTips.this.finish();
            }
        }, 5000);
    }

    public void initializeTips(){
        tipsDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tip = dataSnapshot.getValue(String.class);
                tips.setText(tip);
                Log.e("Loading Tips",tip);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
