package com.wastefier.wastefier;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Segregation extends AppCompatActivity {
    private ImageView img;
    private ImageView greenBin;
    private ImageView redBin;
    private ViewGroup rootLayout;
    private int _xDelta;
    private int _yDelta;
    private Dialog myDialog;
    private int score=0;
    private Button ok;
    private Boolean flag;
    private String answer;
    private String item;
    private TextView textView;
    public int temp;

    private DatabaseReference mDatabase;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segregation);
        Bundle extras = getIntent().getExtras();
        byte[] byteArray = extras.getByteArray("picture");
        answer = extras.getString("answer");
        item = extras.getString("item");
        flag = false;
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        rootLayout = (ViewGroup) findViewById(R.id.view_root);
        img = (ImageView) rootLayout.findViewById(R.id.imageView2);
        greenBin = (ImageView) rootLayout.findViewById(R.id.imageView5);
        redBin = (ImageView) rootLayout.findViewById(R.id.imageView4);
        img.setImageBitmap(bmp);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(600,400);
        layoutParams.setMargins(450,200,0,0);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(user.getUid()).child("score");

        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                temp = Integer.parseInt(dataSnapshot.getValue().toString());
                Log.e("Score",dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        user = firebaseAuth.getCurrentUser();
        img.setLayoutParams(layoutParams);
        img.setOnTouchListener(new ChoiceTouchListener());
    }
    private final class ChoiceTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent event){
            final int X = (int) event.getRawX();
            final int Y = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    _xDelta = X - lParams.leftMargin;
                    _yDelta = Y - lParams.topMargin;
                    img.setImageResource(R.drawable.trash);
                    break;
                case MotionEvent.ACTION_UP:
                    if(Y >= 1650 && Y <= 2107) {
                        if(X >= 275 && X <= 455){
                            //Log.e("dropped","green");
                            Log.e("ANSWER",answer);
                            greenBin.setImageResource(R.drawable.closegreenbin);
                            img.setVisibility(View.GONE);
                            if(answer.toLowerCase().equals("biodegradable")){
                                AnswerDialog("Great! You are right");
                                score = 5;
                            }
                            else{
                                AnswerDialog("Oh oh.. You are wrong");
                                score = 1;
                            }
                        }
                        else if(X >= 934 && X <= 1232){
                            //Log.e("dropped","red");
                            Log.e("ANSWER",answer);
                            redBin.setImageResource(R.drawable.closeredbin);
                            img.setVisibility(View.GONE);
                            if(answer.toLowerCase().equals("nonbiodegradable")){
                                AnswerDialog("Great! You are right");
                                score = 5;
                            }
                            else{
                                AnswerDialog("Oh oh.. You are wrong");
                                score = 1;
                            }
                        }
                        else{
                            redBin.setImageResource(R.drawable.closeredbin);
                            greenBin.setImageResource(R.drawable.closegreenbin);
                        }
                    }
                    else{
                        redBin.setImageResource(R.drawable.closeredbin);
                        greenBin.setImageResource(R.drawable.closegreenbin);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    layoutParams.leftMargin = X - _xDelta;
                    layoutParams.topMargin = Y - _yDelta;
                    layoutParams.rightMargin -= 250;
                    layoutParams.bottomMargin -= 250;
                    Log.e("Xvalue","value == "+X);
                    Log.e("Yvalue","value == "+Y);
                    if(Y >= 1650 && Y <= 2107) {

                        if(X >= 275 && X <= 455){
                            //Log.e("haha","green");
                            greenBin.setImageResource(R.drawable.opengreenbin);
                        }
                        else if(X >= 934 && X <= 1232){
                            //Log.e("haha","red");
                            redBin.setImageResource(R.drawable.openredbin);
                        }
                        else{
                            redBin.setImageResource(R.drawable.closeredbin);
                            greenBin.setImageResource(R.drawable.closegreenbin);
                        }
                    }
                    view.setLayoutParams(layoutParams);
                    break;
            }
            rootLayout.invalidate();
            mDatabase.child("User").child(user.getUid()).child("score").setValue(temp+score);
            return true;
        }
    }

    public void AnswerDialog(String str){
        myDialog = new Dialog(Segregation.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.answerdialog);
        myDialog.setTitle("Result");
        ok = (Button) myDialog.findViewById(R.id.ok);
        ok.setEnabled(true);
        textView = (TextView)myDialog.findViewById(R.id.text123);
            textView.setText(str);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.cancel();
                Intent  intent = new Intent(Segregation.this, MainMenu.class);
                startActivity(intent);
            }
        });
        myDialog.show();
    }
}
