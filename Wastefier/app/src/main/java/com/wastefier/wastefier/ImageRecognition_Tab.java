package com.wastefier.wastefier;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by Sully the Seal on 08/05/2017.
 */

public class ImageRecognition_Tab extends Fragment {
    ImageView imageView;
    Bitmap bitmap;
    private static final int CAMERA_REQUEST = 1888;
    public VisionServiceClient visionServiceClient = new VisionServiceRestClient("45cdde853a6b41bc968fb42c9daaa877","https://westcentralus.api.cognitive.microsoft.com/vision/v1.0");
    private FirebaseAuth firebaseAuth;
    private Dialog verifyDialog;
    private Dialog verify2Dialog;
    private Dialog verify3Dialog;
    private Button yes;
    private Button no;
    private Button next;
    private Button done;
    private EditText picture;
    private EditText madeOf;
    private TextView dialogDescription;
    private TextView textView;
    String answer="";
    String objectName="";
    String made="";
    private boolean flag = false;
    private boolean verifyFlag = false;
    private boolean nextFlag = false;
    Checker c;
    String item="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab_2, container, false);
        FloatingActionButton fab_camera = (FloatingActionButton)rootView.findViewById(R.id.fab_camera);
        Button segregate = (Button) rootView.findViewById(R.id.segregate);
        bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.banana);
        imageView = (ImageView)rootView.findViewById(R.id.imageView);

        imageView.setImageBitmap(bitmap);

        textView = (TextView) rootView.findViewById(R.id.description);
        textView.setText("Description");

        c = new Checker();



        fab_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = true;
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent,CAMERA_REQUEST);
            }
        });

        segregate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Checker c = new Checker();
                //item = c.common(objectName);
                //Log.e("[0]Item",item);
                Log.e("objectName",objectName);
                Log.e("SEGREGATE",answer);
                if(flag) {
                    if (answer == "Unknown") {
                        Snackbar.make(view, "Picture doesn't contain any known trash.", Snackbar.LENGTH_SHORT)
                                .setAction("Action", null).show();
                    }
                    else {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        Intent intent = new Intent(getActivity(), LoadingTips.class);
                        //Log.e(">>>>>>>>", answer.toString());
                        intent.putExtra("answer", answer.toString());
                        intent.putExtra("picture", byteArray);
                        //intent.putExtra("item", item);
                        startActivity(intent);
                    }
                }
                else{
                    Snackbar.make(view, "Take a picture first", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                    }
                }
        });



        return rootView;
    }

    public void run(ByteArrayInputStream inputStream){
        final AsyncTask<InputStream,String,String> visionTask = new AsyncTask<InputStream, String, String>() {
            ProgressDialog dialog = new ProgressDialog(getContext());

            @Override
            protected String doInBackground(InputStream... params) {
                try{
                    publishProgress("Recognizing....");
                    String[] features = {"Description"};
                    String[] details = {};

                    AnalysisResult result = visionServiceClient.analyzeImage(params[0],features,details);

                    String strResult = new Gson().toJson(result);
                    return strResult;

                }catch(Exception e){
                    return null;
                }
            }

            @Override
            protected void onPreExecute(){
                dialog.show();
            }

            @Override
            protected void onPostExecute(String s){
                try {
                    dialog.dismiss();
                    AnalysisResult result = new Gson().fromJson(s, AnalysisResult.class);
                    StringBuilder sb = new StringBuilder();
                    for (Caption caption : result.description.captions) {
                        sb.append(caption.text);
                    }
                    textView.setText(sb);
                    objectName = sb.toString();
                    VerifyDialog(sb.toString());
                    item = c.common(objectName);
                    Log.e(">>>answer",answer);
                }catch(Exception e){
                    Log.e("ERROR",e.toString());
                }
            }

            @Override
            protected void onProgressUpdate(String... values) {
                dialog.setMessage(values[0]);
            }
        };
        visionTask.execute(inputStream);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            bitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(bitmap);
        }
        if(!isNetworkAvailable(getContext())) {
            Toast.makeText(getContext(),"No Internet Connection", Toast.LENGTH_LONG).show();
            return;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,90,outputStream);
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        //Log.e("Running","Code is running");
        //Log.e("Running","Code is running");
        run(inputStream);
    }
    /**
     * This method check mobile is connected to network.
     * @param context
     * @return true if connected otherwise false.
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(conMan.getActiveNetworkInfo() != null && conMan.getActiveNetworkInfo().isConnected())
            return true;
        else
            return false;
    }

    public void VerifyDialog(final String str){
        verifyDialog = new Dialog(this.getContext());
        verifyDialog.setContentView(R.layout.verification);
        verifyDialog.setTitle("Verify Result");
        yes = (Button) verifyDialog.findViewById(R.id.button2);
        yes.setEnabled(true);
        no = (Button) verifyDialog.findViewById(R.id.button3);
        no.setEnabled(true);
        dialogDescription = (TextView)verifyDialog.findViewById(R.id.dialogDesc);
        dialogDescription.setText(str);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyFlag=true;
                verifyDialog.cancel();
                Verify3Dialog();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyDialog.cancel();
                Verify2Dialog();
            }
        });
        verifyDialog.show();
    }
    public void Verify2Dialog(){
        verify2Dialog = new Dialog(ImageRecognition_Tab.this.getContext());
        verify2Dialog.setContentView(R.layout.verification2);
        verify2Dialog.setTitle("Describe Picture");
        picture = (EditText)verify2Dialog.findViewById(R.id.picture);
        //Log.e("OBJECT",picture.getText().toString());
        next = (Button) verify2Dialog.findViewById(R.id.next);
        next.setEnabled(true);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextFlag=true;
                verify2Dialog.cancel();
                objectName = picture.getText().toString();
                Verify3Dialog();
            }
        });
        verify2Dialog.show();
    }
    public void Verify3Dialog(){

        verify3Dialog = new Dialog(this.getContext());
        verify3Dialog.setContentView(R.layout.verification3);
        verify3Dialog.setTitle("What is it made of?");

        madeOf = (EditText)verify3Dialog.findViewById(R.id.made);
        done = (Button) verify3Dialog.findViewById(R.id.done);
        done.setEnabled(true);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verify3Dialog.cancel();
                made = madeOf.getText().toString();
                Log.e("MADE",madeOf.getText().toString());
                verify();
            }
        });
        verify3Dialog.show();
    }

    public void verify(){
        Log.e("OBJECT", objectName.toString());

        answer = c.check(objectName.toString());
        //item = c.common(objectName.toString());
        String temp = c.check(made.toString());
        Log.e("MADE", temp.toLowerCase().toString());
        if (!(temp.toLowerCase().equals(answer.toString().toLowerCase())) && !(made.toString().toLowerCase().equals("real"))) {
            answer = c.check(made.toString());
            //item = c.common(made.toString());
            Log.e("INSIDE", answer);
            Log.e("ANSWER", answer);
            textView.setText(objectName.toString() + " made of " + made.toString().toLowerCase());
            return;
        }
        textView.setText(objectName.toString());
        Log.e("ANSWER", answer);
    }

}
