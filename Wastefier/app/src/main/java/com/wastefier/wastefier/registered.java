package com.wastefier.wastefier;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class registered extends AppCompatActivity {


    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered);
        TextView name = (TextView) findViewById(R.id.username);
        Bundle bundle = getIntent().getExtras();
        String username = bundle.getString("message");
        name.setText(username);

        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = firebaseAuth.getCurrentUser();
        UserAccount acc = new UserAccount(username, "0");
        Log.e("CREATE UID,",user.getUid());
        mDatabase.child("User").child(user.getUid()).setValue(acc);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent mainIntent = new Intent(registered.this, MainMenu.class);
                registered.this.startActivity(mainIntent);
                registered.this.finish();
            }
        }, 5000);
    }
}
